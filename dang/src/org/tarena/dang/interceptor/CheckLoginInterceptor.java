package org.tarena.dang.interceptor;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class CheckLoginInterceptor extends AbstractInterceptor{
	//登录检查
	public String intercept(ActionInvocation invocation) throws Exception {
		//System.out.println("正在进行登陆检查...");
		//判断session中是否有登录信息
		Map<String,Object> session = 
			ActionContext.getContext().getSession();
		if(session.get("user") == null){
			//未登录,跳转到login.jsp登录页面
			return "login";
		}else{
			//已登录,调用Action业务方法
			String resultCode =  invocation.invoke();//调用了action和result
			//拦截器后续处理
			return resultCode;
		}
	}
}
