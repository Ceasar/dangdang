package org.tarena.dang.interceptor;

import org.tarena.dang.util.DbUtil;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class TransactionInterceptor extends AbstractInterceptor{
	/**
	 * 事务管理
	 */
	public String intercept(ActionInvocation invocation) throws Exception {
		//开启事务控制，关闭自动提交
		DbUtil.beginTransaction();
		System.out.println("开始事务控制");
		try{
			//调用Action,Result
			invocation.invoke();
			//提交或回滚事务
			DbUtil.commit();
			System.out.println("提交事务");
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			DbUtil.rollback();//回滚
			System.out.println("回滚事务");
			throw ex;
		}finally{
			DbUtil.closeConnection();
		}
		
	}
	
}
