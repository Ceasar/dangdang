package org.tarena.dang.service;

import java.util.List;

import org.tarena.dang.entity.CartItem;

public interface CartService {
	public boolean buy(int pid) throws Exception;
	public void modify(int pid,int qty) throws Exception;
	public void delete(int pid) throws Exception;
	public void recovery(int pid) throws Exception;
	/**
	 * 统计“确认购买”的商品金额
	 * @return
	 * @throws Exception
	 */
	public double cost() throws Exception;//花费 金额总计 确认购买的产品金额
	/**
	 * 统计“确认购买”的商品节省金额
	 * @return
	 * @throws Exception
	 */
	public double sale() throws Exception;//确认购买的节省金额
	/**
	 * 获取确认购买商品显示列表
	 * @return
	 * @throws Exception
	 */
	public List<CartItem> getBuyPros() throws Exception;
	/**
	 * 获取取消购买的商品显示列表
	 * @return
	 * @throws Exception
	 */
	public List<CartItem> getDelPros() throws Exception;
	//删除购物车中所有商品
	public void deleteAll() throws Exception;
	
	public void load(String content);
	
	public String store();
}

