package org.tarena.dang.service;

import java.util.ArrayList;
import java.util.List;

import org.tarena.dang.dao.JdbcProductDAO;
import org.tarena.dang.dao.ProductDAO;
import org.tarena.dang.entity.CartItem;
import org.tarena.dang.entity.Product;

public class CartServiceImpl implements CartService{
	private List<CartItem> store = new ArrayList<CartItem>();
	
	public boolean buy(int pid) throws Exception {
		//判断购物车是否已购买
		for(CartItem item:store){
			if(item.getPro().getId()==pid){
				//如果已购买返回false
				return false;
			}
		}
		//未购买过,封装成CartItem添加到集合
		ProductDAO dao = new JdbcProductDAO();
		Product pro = dao.findById(pid);
		CartItem item = new CartItem();
		item.setPro(pro);
		//item.setQty(1);//默认值已经设置为1
		//item.setBuy(true);//默认值已经设置为true
		store.add(item);
		return true;
	}
	public void delete(int pid) throws Exception {
		for(CartItem item : store){
			if(item.getPro().getId() == pid){
				item.setBuy(false);
			}
		}
	}

	public List<CartItem> getBuyPros() throws Exception {
		List<CartItem> l = new ArrayList<CartItem>();
		for(CartItem item : store){
			if(item.isBuy()){
				l.add(item);
			}
		}
		return l;
	}

	public List<CartItem> getDelPros() throws Exception {
		List<CartItem> l = new ArrayList<CartItem>();
		for(CartItem item : store){
			if(item.isBuy()==false){
				l.add(item);
			}
		}
		return l;
	}

	public void modify(int pid, int qty) throws Exception {
		for(CartItem item:store){
			if(item.getPro().getId()==pid){
				item.setQty(qty);
			}
		}
	}

	public void recovery(int pid) throws Exception {
		for(CartItem item : store){
			if(item.getPro().getId() == pid){
				item.setBuy(true);
			}
		}
	}
	
	public double cost() throws Exception {
		double totle = 0;
		for(CartItem item:store){
			if(item.isBuy()){
				totle += item.getPro().getDangPrice()*item.getQty();				
			}
		}
		return totle;
	}
	
	public double sale() throws Exception {
		double dangTotle = cost();
		double fixTotle = 0;
		for(CartItem item : store){
			if(item.isBuy()){
				fixTotle += item.getPro().getFixedPrice()*item.getQty();
			}
		}
		return fixTotle-dangTotle;
	}
	public void deleteAll() throws Exception {
		store.clear();
	}
	public void load(String content) {
		if(content == null || content.equals("0")){
			//如果之前的购物车没有任何商品，则不需要恢复
			return;
		}
		String[] contents = content.split(";");
		for(int i=0;i<contents.length;i++){
			String itemInfo = contents[i];
			String[] infos = itemInfo.split(",");
			int id = Integer.parseInt(infos[0]);
			int qty = Integer.parseInt(infos[1]);
			boolean buy = Boolean.parseBoolean(infos[2]);
			ProductDAO dao = new JdbcProductDAO();
			try {
				Product p = dao.findById(id);
				CartItem item = new CartItem();
				item.setPro(p);
				item.setQty(qty);
				item.setBuy(buy);
				store.add(item);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 将items转换成一个形如
	 * 3,8;2,10;7,6这样的字符串。
	 * 其中，3表示id,8表示数量,依此类推。
	 * 如果返回0，则表示没有任何的商品。
	 * @return
	 */
	public String store(){
		StringBuffer buf = new StringBuffer();
		if(store.size() == 0){
			buf.append("0");
		}else{
			for(int i=0;i<store.size();i++){
				CartItem item = store.get(i);
				buf.append(item.getPro().getId()
						+"," + item.getQty() +","+ item.isBuy() + ";");
			}
		}
		if(buf.length() > 1){
			buf.deleteCharAt(buf.length()-1);
		}
		return buf.toString();
	}
}
