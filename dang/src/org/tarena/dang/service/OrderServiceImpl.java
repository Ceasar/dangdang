package org.tarena.dang.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tarena.dang.dao.OrderDAO;
import org.tarena.dang.entity.Address;
import org.tarena.dang.entity.CartItem;
import org.tarena.dang.entity.Order;
import org.tarena.dang.entity.User;
import org.tarena.dang.util.Constant;
import org.tarena.dang.util.Factory;

import com.opensymphony.xwork2.ActionContext;

public class OrderServiceImpl implements OrderService{
	private boolean newAddress = true;
	public int saveOrder(Order order) throws Exception {
		OrderDAO dao = (OrderDAO)Factory.getInstance("OrderDAO");
		Map<String,Object> session = ActionContext.getContext().getSession();
		User user = (User) session.get("user");
		order.setUserId(user.getId());
		order.setStatus(Constant.STATUS);
		order.setOrderTime(System.currentTimeMillis());
		CartService cartService = CartFactory.getInstance(session);
		order.setTotalPrice(cartService.cost());
		//存到d_order
		dao.saveOrder(order);
		//获取产品信息
		List<CartItem> itemsBuy = new ArrayList<CartItem>();
		itemsBuy = (List<CartItem>) session.get(Constant.CARTISBUY);
		//得到OrderId
		int orderId = dao.findOrderId(user.getId());
		//保存到d_item中
		dao.saveItem(orderId,itemsBuy);
		//保存用户地址到数据库d_receive_address
		List<Address> addresses = dao.findAddressById(user.getId());
		if(addresses.size()==0){
			dao.saveReceiveAddress(order);
		}else{
			for(Address address:addresses){
			newAddress=(address.getFullAddress().equals(order.getFullAddress()))
					&&(address.getMobile().equals(order.getMobile())
					&&address.getPhone().equals(order.getPhone())
					&&address.getPostalCode().equals(order.getPostalCode())
					&&address.getReceiveName().equals(order.getReceiveName()));
					if(newAddress){
						break;
					}
			}
			if(!newAddress){
				dao.saveReceiveAddress(order);
			}
		}
		return orderId;
	}

	//通过userId找到用户的所有地址
//	public List<Address> findAddress(int userId) throws Exception {
//		OrderDAO dao =(OrderDAO) Factory.getInstance("OrderDAO");
//		return dao.findAddress(userId);
//	}
}
