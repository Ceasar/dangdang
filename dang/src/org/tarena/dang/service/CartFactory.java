package org.tarena.dang.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.tarena.dang.util.Constant;
import org.tarena.dang.util.CookieUtil;

public class CartFactory {
	public static CartService getInstance(Map<String,Object> session) throws Exception{
		CartService cart = (CartService)session.get(Constant.CART);
		//如果session没有就新建一个
		if(cart==null){
			cart = new CartServiceImpl();
			//尝试从cookie恢复
			//有可能用户之前已经购买过一些商品，这些商品
			//信息已经以cookie的形式保存在了客户端，需要先恢复	
			HttpServletRequest request = ServletActionContext.getRequest();
			String content = CookieUtil.findCookie("cart",request);
System.out.println("CartService cookie content:"+ content );
			cart.load(content);
			//存入session
			session.put(Constant.CART,cart);
		}
		return cart;
	}
}
