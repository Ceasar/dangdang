package org.tarena.dang.service;

import java.util.ArrayList;
import java.util.List;

import org.tarena.dang.dao.CategoryDAO;
import org.tarena.dang.dao.JdbcCategoryDAO;
import org.tarena.dang.entity.Category;

public class MainServiceImpl implements MainService{

	public List<Category> findLeftCategory() throws Exception {
		CategoryDAO catDao = new JdbcCategoryDAO();
		List<Category> all = catDao.findAll();
		//返回parentId=1的Category对象
		List<Category> cats = findByParentId(1,all);
		//加载cats集合元素的子类别
		for(Category cat : cats){
			List<Category> subCats = findByParentId(cat.getId(),all);
			cat.setSubCats(subCats);
		}
		return cats;
	}
	
	private List<Category> findByParentId(int pid,List<Category> all){
		List<Category> subCats = new ArrayList<Category>();
		for(Category cat : all){
			if(cat.getParentId() == pid){
				subCats.add(cat);
			}
		}
		return subCats;
	}
	
}
