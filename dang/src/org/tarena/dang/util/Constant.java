package org.tarena.dang.util;

public final class Constant {
	//用户等级
	public static final int NORMAL = 0;
	public static final int HIGNHT = 1;
	public static final int VIP = 2;
	//邮箱验证标识
	public static final String YES = "Y";
	public static final String NO = "N";
	//Session的key
	public static final String CART = "cart.key";
	public static final String CARTISBUY="cart.isbuy";
	
	public final static int STATUS=1;
}
