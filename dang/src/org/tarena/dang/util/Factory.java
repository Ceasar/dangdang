package org.tarena.dang.util;


public class Factory {
	public static Object getInstance(String type){
		Object obj = null;
		//依据type找到对应的类名
		String className = ConfigUtil.getValue(type);
		//依据反射机制，创建对象
		try {
			//加载某个类
			Class c = Class.forName(className);
			//调用class对象的方法来创建一个对象
			obj = c.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	public static void main(String[] args) throws Exception {
		System.out.println(getInstance("OrderDAO"));
	}
}
