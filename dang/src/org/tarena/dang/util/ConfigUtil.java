package org.tarena.dang.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 将dao.properties文件的内容加载进来，
 * 提供相应的方法来访问配置信息。
 * @author Administrator
 *
 */
public class ConfigUtil {
	private static Properties props = 
		new Properties();
	static{
		//完成dao.properties文件的读取工作
		ClassLoader loader = ConfigUtil.class
		.getClassLoader();
		InputStream ips =loader	
			.getResourceAsStream("dao.properties");
		try {
			props.load(ips);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getValue(String key){
		return props.getProperty(key);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String value = getValue("CategoryDAO");
		System.out.println(value);

	}

}
