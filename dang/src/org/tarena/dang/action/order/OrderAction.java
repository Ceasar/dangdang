package org.tarena.dang.action.order;

import java.util.List; 

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.dao.OrderDAO;
import org.tarena.dang.entity.Address;
import org.tarena.dang.entity.CartItem;
import org.tarena.dang.entity.Order;
import org.tarena.dang.entity.User;
import org.tarena.dang.service.CartFactory;
import org.tarena.dang.service.CartService;
import org.tarena.dang.service.OrderService;
import org.tarena.dang.util.Factory;

public class OrderAction extends BaseAction{
	//input
	private Order order;
	private int addressId;
	//output
	private List<CartItem> cartItems;//取消时返回用的列表
	private List<Address> addressList;//点击下拉框 出现的所有地址
	private Address selectAddress;//选上曾经用过的地址
	private int orderId;//订单id
	private double cost;//总金额
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public Address getSelectAddress() {
		return selectAddress;
	}
	public void setSelectAddress(Address selectAddress) {
		this.selectAddress = selectAddress;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public List<CartItem> getCartItems() {
		return cartItems;
	}
	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}
	public List<Address> getAddressList() {
		return addressList;
	}
	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	//点击取消
	public String orderInfo() throws Exception{
		CartService cart = CartFactory.getInstance(session);
		cartItems = cart.getBuyPros();//确认购买的列表
		cost = cart.cost();
		return "orderInfo";
	}
	//把所用东西都往表里面存
	public String createOrder() throws Exception{
		OrderService orderService = (OrderService) Factory.getInstance("OrderService");
		CartService cartService = CartFactory.getInstance(session);
		orderId = orderService.saveOrder(order);
		cost = cartService.cost();
		cartService.deleteAll(); 
		return "address";
	}
	//通过用户Id添加新地址下拉框
	public String loadAddress() throws Exception{
		OrderDAO dao = (OrderDAO) Factory.getInstance("OrderDAO");
		User user = (User) session.get("user");
		addressList = dao.findAddressById(user.getId());
		return "load";
	}
	
	//加载已存地址的详细信息
	public String findSelectAddress() throws Exception{
		OrderDAO dao = (OrderDAO) Factory.getInstance("OrderDAO");
		User user = (User) session.get("user");
		selectAddress = dao.findSelectAddress(user.getId(),addressId);
		return "loadAddrInfo";
	}
}
