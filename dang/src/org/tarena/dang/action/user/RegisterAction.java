package org.tarena.dang.action.user;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.dao.JdbcUserDAO;
import org.tarena.dang.dao.UserDAO;
import org.tarena.dang.entity.User;
import org.tarena.dang.util.Constant;
import org.tarena.dang.util.EmailUtil;
import org.tarena.dang.util.EncryptUtil;
import org.tarena.dang.util.VerifyUtil;

public class RegisterAction extends BaseAction{
	//input
	private User user;
	private String email;
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String execute() throws Exception{
		System.out.println("User:"+user);
		//密码加密
		String pwd = EncryptUtil.md5Encrypt(user.getPassword());
		user.setPassword(pwd);
		//设置用户初始信息
		user.setUserIntegral(Constant.NORMAL);
		user.setEmailVerify(Constant.NO);
		user.setLastLoginTime(System.currentTimeMillis());
		user.setLastLoginIp(httpReq.getRemoteAddr());
		//生成一个邮箱验证码
		String code = VerifyUtil.createCode();
		user.setEmailVerifyCode(code);
		//将user写入d_user
		UserDAO userDao = new JdbcUserDAO();
		userDao.save(user);
		//发送邮箱验证码
		EmailUtil.sendEmail(user.getEmail(), code);
		return "verify";//进入邮箱验证页面
	}
	public String confirm() throws Exception{
		UserDAO userDao = new JdbcUserDAO();
		user = userDao.findByEmail(email);
		return "ok";		
	}
}
