package org.tarena.dang.action.user;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.dao.JdbcUserDAO;
import org.tarena.dang.dao.UserDAO;
import org.tarena.dang.entity.User;
import org.tarena.dang.util.Constant;

public class CheckEmailCodeAction extends BaseAction {
	//input
	private String code;
	private String email;
//	private String reEmail;
//	public String getReEmail() {
//		return reEmail;
//	}
//	public void setReEmail(String reEmail) {
//		this.reEmail = reEmail;
//	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	//output
	private boolean ok1 = false;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isOk1() {
		return ok1;
	}
	public void setOk1(boolean ok1) {
		this.ok1 = ok1;
	}
	public String execute() throws Exception{	
		UserDAO dao = new JdbcUserDAO();
		User user = dao.findByEmail(email);
		if(user.getEmailVerifyCode().equals(code)){
			ok1 = true;
		}else{
			ok1 = false;
		}
		dao.changeVerify(email);
		return "success";
	}
}
