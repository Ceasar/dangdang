package org.tarena.dang.action.user;

import org.tarena.dang.action.BaseAction;

public class CheckCodeAction extends BaseAction{
	//input
	private String number;
	//output
	private boolean ok = false;

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	public String execute(){
		//获取session存储的code
		String code = (String) session.get("code");
		//比较code和number
		if(code.equalsIgnoreCase(number)){
			ok = true;
		}else{
			ok = false;
		}
		return "success";
		
	}
}
