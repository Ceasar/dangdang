package org.tarena.dang.action.user;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Map;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.util.ImageUtil;


import com.opensymphony.xwork2.ActionContext;

public class ImageCodeAction extends BaseAction{
	//output
	private  InputStream imageStream;
	
	public String execute() throws Exception{
			//动态生成一个图片
			Map<String,BufferedImage> map= ImageUtil.createImage();
			//获取图片字符，写入session，为校验做准备
			String code = map.keySet().iterator().next();//获取一串的key
			session.put("code",code);
			//将图片对象给imageStream赋值
			BufferedImage image = map.get(code);
			imageStream = ImageUtil.getInputStream(image);
			return "success";//交给stream类型result负责输出
	}
	
	public InputStream getImageStream() {
		return imageStream;
	}

	public void setImageStream(InputStream imageStream) {
		this.imageStream = imageStream;
	}
	
}
