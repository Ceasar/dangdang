package org.tarena.dang.action.user;

import org.tarena.dang.dao.JdbcUserDAO;
import org.tarena.dang.dao.UserDAO;
import org.tarena.dang.entity.User;

public class CheckEmailAction {
	//input
	private String email;
	//output
	private boolean ok = false;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	
	public String execute() throws Exception{
		UserDAO userDao = new JdbcUserDAO();
		User user = userDao.findByEmail(email);
		if(user == null){
			ok = true;//可用
		}else{
			ok = false;//被占用
		}
		return "success";
	}
	
}
