package org.tarena.dang.action.user;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.dao.JdbcUserDAO;
import org.tarena.dang.dao.UserDAO;
import org.tarena.dang.entity.User;
import org.tarena.dang.util.EncryptUtil;

public class LoginAction extends BaseAction{
	//input
	private String name;
	private String password;
	//output
	private String error_m;
	
	public String getError_m() {
		return error_m;
	}
	public void setError_m(String errorM) {
		error_m = errorM;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String execute() throws Exception{
		UserDAO dao = new JdbcUserDAO();
		User user = dao.findByEmail(name);
		String pwd = EncryptUtil.md5Encrypt(password);
		if(user!=null&&user.getPassword().equals(pwd)){
			session.put("user",user);
			if(user.getEmailVerify().equals("N")){
				return "check";
			}
			return "success";
		}else{
			error_m="�û������������";
			return "loginError";
		}
	}
}
