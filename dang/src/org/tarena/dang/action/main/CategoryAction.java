package org.tarena.dang.action.main;

import java.util.List;

import org.tarena.dang.entity.Category;
import org.tarena.dang.service.MainService;
import org.tarena.dang.service.MainServiceImpl;

public class CategoryAction {
	//input
	//output
	//����parentId=1��Category����
	private List<Category> cats;
	
	public String execute() throws Exception{
		MainService service = new MainServiceImpl();
		cats = service.findLeftCategory();
		return "success";
	}

	public List<Category> getCats() {
		return cats;
	}

	public void setCats(List<Category> cats) {
		this.cats = cats;
	}
	
}