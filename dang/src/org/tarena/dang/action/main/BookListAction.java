package org.tarena.dang.action.main;

import java.util.List;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.dao.CategoryDAO;
import org.tarena.dang.dao.JdbcCategoryDAO;
import org.tarena.dang.dao.JdbcProductDAO;
import org.tarena.dang.dao.ProductDAO;
import org.tarena.dang.entity.Book;
import org.tarena.dang.entity.Category;
import org.tarena.dang.entity.User;

public class BookListAction extends BaseAction{
	//input
	private int pid;//父类别id
	private int cid;//当前类别ID
	private int page = 1;//显示第几页，默认第一页
	//output
	private List<Category> cats;//根据pid获取的子类别
	private int totalNum;
	private List<Book> books;//中间产品列表信息
	
	private int totalPages;//总页数
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	//injection
	private int pageSize;//每页显示3条
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public List<Book> getBooks() {
		return books;
	}
	public void setBooks(List<Book> books) {
		this.books = books;
	}
	public int getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public List<Category> getCats() {
		return cats;
	}
	public void setCats(List<Category> cats) {
		this.cats = cats;
	}
	
	public String execute() throws Exception{
		//根据pid查询cats数据
		CategoryDAO catDao = new JdbcCategoryDAO();
		cats = catDao.findByParentId(pid);
		//统计全部数量
		for(Category cat:cats){
			totalNum+=cat.getPnum();
		}
		//根据cid获取books集合(中间产品显示)
		ProductDAO proDao = new JdbcProductDAO();
		books = proDao.findBooksByCatId(cid,page,pageSize);
		totalPages = proDao.getTotalPages(cid, pageSize);
		User user = (User)session.get("user");
//		System.out.println("得到的"+user);
		return "success";
	}
}
