package org.tarena.dang.action.main;

import java.util.List;

import org.tarena.dang.dao.JdbcProductDAO;
import org.tarena.dang.dao.ProductDAO;
import org.tarena.dang.entity.Product;

public class RankingAction {
	//input
	private int pid;
	//output
	private List<Product> topList;
	private Product pro;
	
	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public List<Product> getTopList() {
		return topList;
	}

	public void setTopList(List<Product> topList) {
		this.topList = topList;
	}
	
	public Product getPro() {
		return pro;
	}

	public void setPro(Product pro) {
		this.pro = pro;
	}

	public String execute() throws Exception{
		ProductDAO dao = new JdbcProductDAO();
		topList = dao.findTopTen();
		return "success";
	}
	//找到图书的信息
	public String findProduct() throws Exception{
		ProductDAO dao = new JdbcProductDAO();
		pro = dao.findById(pid);
		return "success";
	}
}
