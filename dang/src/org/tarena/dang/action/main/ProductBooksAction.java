package org.tarena.dang.action.main;

import java.util.List;
import java.util.Random;

import org.tarena.dang.dao.JdbcProductDAO;
import org.tarena.dang.dao.ProductDAO;
import org.tarena.dang.entity.Book;
import org.tarena.dang.entity.Product;
import org.tarena.dang.entity.ProductHot;


public class ProductBooksAction {
	private int size = 8;
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	private List<Product> list;
	private Book rec1;
	private Book rec2;
	private List<ProductHot> list2;

	public List<ProductHot> getList2() {
		return list2;
	}

	public void setList2(List<ProductHot> list2) {
		this.list2 = list2;
	}


	public Book getRec1() {
		return rec1;
	}

	public void setRec1(Book rec1) {
		this.rec1 = rec1;
	}

	public Book getRec2() {
		return rec2;
	}

	public void setRec2(Book rec2) {
		this.rec2 = rec2;
	}

	public List<Product> getList() {
		return list;
	}

	public void setList(List<Product> list) {
		this.list = list;
	}

	public String newBooks() throws Exception{
		ProductDAO dao = new JdbcProductDAO();
		list = dao.findNewAll(size);
		return "new";
	}
	public String recommendBooks() throws Exception{
		ProductDAO dao = new JdbcProductDAO();
		List<Book> list = dao.findRecommendAll();
		rec1 = list.get(new Random().nextInt(list.size()));
		list.remove(rec1);
		rec2 = list.get(new Random().nextInt(list.size()));
		return "recommend";
	}
	
	public String hotBooks() throws Exception{
		ProductDAO dao = new JdbcProductDAO();
		list2 = dao.fintHotAll();
		return "hot";
	}
}
