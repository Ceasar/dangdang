package org.tarena.dang.action.main;

import org.tarena.dang.action.BaseAction;

public class RemoveSessionAction extends BaseAction{
	public String execute(){
		session.remove("user");
		return "success";
	}
}
