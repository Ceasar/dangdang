package org.tarena.dang.action.cart;

import java.util.List;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.entity.CartItem;
import org.tarena.dang.service.CartFactory;
import org.tarena.dang.service.CartService;

public class CartOrderAction extends BaseAction{
	//output
	private List<CartItem> cartItems;
	private double cost;

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public List<CartItem> getCartItems() {
		return cartItems;
	}

	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}
	//生成确认订单列表
	public String execute() throws Exception{
		CartService cartService = CartFactory.getInstance(session);		
		cartItems =cartService.getBuyPros();
		cost = cartService.cost();
		return "success";
	}
	public String next() throws Exception{
		System.out.println("sss");
		return "success";
	}
}
