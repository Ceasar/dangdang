package org.tarena.dang.action.cart;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.service.CartFactory;
import org.tarena.dang.service.CartService;
import org.tarena.dang.util.CookieUtil;

public class AddAction extends BaseAction{
	//input
	private int pid;
	//output--��json��ʽ����
	private boolean buy=false;
	public String execute() throws Exception{
		CartService cart = CartFactory.getInstance(session);
		buy = cart.buy(pid);
		return "success";
	}
	
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public boolean isBuy() {
		return buy;
	}
	public void setBuy(boolean buy) {
		this.buy = buy;
	}
}
