package org.tarena.dang.action.cart;

import java.util.List;

import org.tarena.dang.action.BaseAction;
import org.tarena.dang.entity.CartItem;
import org.tarena.dang.service.CartFactory;
import org.tarena.dang.service.CartService;
import org.tarena.dang.util.Constant;

public class ShowAction extends BaseAction{
	//input 
	private int id;
	//output
	private List<CartItem> cartItems;
	private double cost;
	private double sale;
	private List<CartItem> delectCartItems;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<CartItem> getDelectCartItems() {
		return delectCartItems;
	}
	public void setDelectCartItems(List<CartItem> delectCartItems) {
		this.delectCartItems = delectCartItems;
	}
	public double getSale() {
		return sale;
	}
	public void setSale(double sale) {
		this.sale = sale;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public List<CartItem> getCartItems() {
		return cartItems;
	}
	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}
	public String execute() throws Exception{
		CartService  cart= CartFactory.getInstance(session);
		delectCartItems = cart.getDelPros();
		cartItems = cart.getBuyPros();
		cost = cart.cost();
		sale = cart.sale();
		session.put(Constant.CARTISBUY, cartItems);
		return "success";
	}
	public String delete() throws Exception{
		CartService cart = CartFactory.getInstance(session);
		cart.delete(id);
		return "delete";
	}
}
