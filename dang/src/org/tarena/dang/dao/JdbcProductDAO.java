package org.tarena.dang.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.tarena.dang.entity.Book;
import org.tarena.dang.entity.Product;
import org.tarena.dang.entity.ProductHot;
import org.tarena.dang.util.DbUtil;

public class JdbcProductDAO implements ProductDAO{
	private static final String findNew = "select * from d_product where has_deleted=0 " +
		"order by add_time desc limit 0,?";
	private static final String findRecommend = "select * from d_book b inner " +
			"join d_product p on b.id = p.id";
	private static final String findHot="select * from d_item i inner " +
			"join d_product p on i.product_id = p.id order by i.product_num desc limit 10";
	private static final String topTen="select * from d_product limit 0,5";
	private static final String findBooksByCatId = 
			"select dp.*,db.*  " +
		    "from d_category_product dcp   " +
		    "      join d_product dp on(dcp.product_id = dp.id)  " +
		    "      join d_book db on(dp.id=db.id)  " +
		    "where dcp.cat_id=? " +
		    "limit ?,?";
	private static final String findById="select * from d_product where id=?";
	
	public List<Product> findNewAll(int size) throws Exception {
		List<Product> newList = new ArrayList<Product>();
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(findNew);
		stat.setInt(1, size);
		ResultSet rs = stat.executeQuery();
		while(rs.next()){
			Product p = new Product();
			p.setId(rs.getInt("id"));
			p.setProductName(rs.getString("product_name"));
			p.setDescription(rs.getString("description"));
			p.setAddTime(rs.getLong("add_time"));
			p.setFixedPrice(rs.getDouble("fixed_price"));
			p.setDangPrice(rs.getDouble("dang_price"));
			p.setKeywords(rs.getString("keywords"));
			p.setHasDeleted(rs.getInt("has_deleted"));
			p.setProductPic(rs.getString("product_pic"));
			newList.add(p);
		}
			DbUtil.closeConnection();
			return newList;
		}
	public List<Book> findRecommendAll() throws Exception {
		List<Book> recommendList = new ArrayList<Book>();
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(findRecommend);
		ResultSet rs = stat.executeQuery();
		while(rs.next()){
			Book pro = new Book();
			//����product���ֶ�
			pro.setId(rs.getInt("id"));
			pro.setProductName(rs.getString("product_name"));
			pro.setDescription(rs.getString("description"));
			pro.setAddTime(rs.getLong("add_time"));
			pro.setFixedPrice(rs.getDouble("fixed_price"));
			pro.setDangPrice(rs.getDouble("dang_price"));
			pro.setKeywords(rs.getString("keywords"));
			pro.setHasDeleted(rs.getInt("has_deleted"));
			pro.setProductPic(rs.getString("product_pic"));
			//����book���ֶ�
			pro.setAuthor(rs.getString("author"));
			pro.setPublishing(rs.getString("publishing"));
			pro.setPublishTime(rs.getLong("publish_time"));
			pro.setCatalogue(rs.getString("catalogue"));
			recommendList.add(pro);
		}
		DbUtil.closeConnection();
		return recommendList;
	}
	public List<ProductHot> fintHotAll() throws Exception {
		List<ProductHot> hotList = new ArrayList<ProductHot>();
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(findHot);
		ResultSet rs = stat.executeQuery();
		while(rs.next()){
			ProductHot ph = new ProductHot();
			ph.setProductName(rs.getString("product_name"));
			ph.setFixedPrice(rs.getDouble("fixed_price"));
			ph.setDangPrice(rs.getDouble("dang_price"));
			ph.setProductPic(rs.getString("product_pic"));
			hotList.add(ph);
		}
		DbUtil.closeConnection();
		return hotList;
	}
	/**
	 * ��ѯ��ҳ��
	 */
	public int getTotalPages(int cid, int size) throws Exception {
		int totalPages = 0;
		StringBuffer sql = new StringBuffer(); 
		sql.append("select count(*) ");
		sql.append("from d_category_product dcp join d_product dp ");
		sql.append("on dcp.product_id=dp.id ");
		sql.append("join d_book db ");
		sql.append("on dp.id=db.id ");
		sql.append("where dcp.cat_id=? ");
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(sql.toString());
		stat.setInt(1, cid);
		ResultSet rs = stat.executeQuery();
		if(rs.next()){
			int totalRows = rs.getInt(1);
			if(totalRows%size==0){
				totalPages = totalRows/size;
			}else{
				totalPages = totalRows/size + 1;
			}
		}
		return totalPages;
	}
	public List<Book> findBooksByCatId(int cid, int page, int size)
			throws Exception {
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(findBooksByCatId);
		stat.setInt(1,cid);
		int begin = (page-1)*size;
		stat.setInt(2,begin);
		stat.setInt(3,size);
		ResultSet rs = stat.executeQuery();
		List<Book> list = new ArrayList<Book>();
		while(rs.next()){
			Book pro = new Book();
			//����product���ֶ�
			pro.setId(rs.getInt("id"));
			pro.setProductName(rs.getString("product_name"));
			pro.setDescription(rs.getString("description"));
			pro.setAddTime(rs.getLong("add_time"));
			pro.setFixedPrice(rs.getDouble("fixed_price"));
			pro.setDangPrice(rs.getDouble("dang_price"));
			pro.setKeywords(rs.getString("keywords"));
			pro.setHasDeleted(rs.getInt("has_deleted"));
			pro.setProductPic(rs.getString("product_pic"));
			//����book���ֶ�
			pro.setAuthor(rs.getString("author"));
			pro.setPublishing(rs.getString("publishing"));
			pro.setPublishTime(rs.getLong("publish_time"));
			list.add(pro);
		}
		return list;
	}
	public List<Product> findTopTen() throws Exception {
		List<Product> list = new ArrayList<Product>();
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(topTen);
		ResultSet rs =stat.executeQuery();
		while(rs.next()){
			Product pro = new Product();
			pro.setId(rs.getInt("id"));
			pro.setProductName(rs.getString("product_name"));
			
			list.add(pro);
		}
		return list;
	}
	public Product findById(int pid) throws Exception {
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(findById);
		stat.setInt(1,pid);
		ResultSet rs = stat.executeQuery();
		Product p = null;
		if(rs.next()){
			p = new Product();
			p.setId(rs.getInt("id"));
			p.setProductName(rs.getString("product_name"));
			p.setDescription(rs.getString("description"));
			p.setAddTime(rs.getLong("add_time"));
			p.setFixedPrice(rs.getDouble("fixed_price"));
			p.setDangPrice(rs.getDouble("dang_price"));
			p.setKeywords(rs.getString("keywords"));
			p.setHasDeleted(rs.getInt("has_deleted"));
			p.setProductPic(rs.getString("product_pic"));
		}
		return p;
	}
}
