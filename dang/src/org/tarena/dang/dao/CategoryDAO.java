package org.tarena.dang.dao;

import java.util.List;

import org.tarena.dang.entity.Category;

public interface CategoryDAO {
	public List<Category> findAll() throws Exception;
	/**
	 *根据父类别ID查询子类别信息 
	 * 
	 */
	public List<Category> findByParentId(int pid) throws Exception;
}
