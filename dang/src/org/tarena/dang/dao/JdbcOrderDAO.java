package org.tarena.dang.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.tarena.dang.entity.Address;
import org.tarena.dang.entity.CartItem;
import org.tarena.dang.entity.Order;
import org.tarena.dang.util.DbUtil;

public class JdbcOrderDAO implements OrderDAO{
	private final static String saveorder="insert into d_order" +
	" ( status,order_desc,total_price,receive_name," +
	"full_address,postal_code,mobile,phone,order_time,user_id)" +
	"values(?,?,?,?,?,?,?,?,?,?)";
	public void saveOrder(Order order) throws Exception {
		Connection conn = DbUtil.getConnection();
		PreparedStatement ps = conn.prepareStatement(saveorder);
		ps.setInt(1, order.getStatus());
		ps.setString(2, order.getOrderDesc());
		ps.setDouble(3, order.getTotalPrice());
		ps.setString(4, order.getReceiveName());
		ps.setString(5, order.getFullAddress());
		ps.setString(6, order.getPostalCode());
		ps.setString(7, order.getMobile());
		ps.setString(8, order.getPhone());
		ps.setLong(9, order.getOrderTime());
		ps.setInt(10, order.getUserId());
		ps.executeUpdate();
	}
	//通过userId找到orderId
	private final static  String findOrderId="select max(id) id from d_order where user_id = ? ";
	public int findOrderId(int userId) throws Exception {
		Connection conn = DbUtil.getConnection();
		PreparedStatement ps = conn.prepareStatement(findOrderId);
		ps.setInt(1, userId);
		ResultSet rs =ps.executeQuery();
		int orderId=0;
		if(rs.next()){
			orderId=rs.getInt("id");
		}
		return orderId;
	}
	//通过userId找到用户的所有地址
	private final static String findAddress="select * from d_receive_address where user_id=? ";
	public List<Address> findAddressById(int userId) throws Exception {
		List<Address> address = new ArrayList<Address>();
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(findAddress);
		stat.setInt(1,userId);
		ResultSet rs = stat.executeQuery();
		while(rs.next()){
			Address add = new Address();
			add.setId(rs.getInt("id"));
			add.setFullAddress(rs.getString("full_address"));
			add.setReceiveName(rs.getString("receive_name"));
			add.setPostalCode(rs.getString("postal_code"));
			add.setMobile(rs.getString("mobile"));
			add.setPhone(rs.getString("phone"));
			add.setUserId(userId);
			address.add(add);
		}
		return address;
	}
	//将产品信息放到数据库d_item
	private final static String saveItem="insert into d_item(order_id,product_id,product_name," +
	"dang_price,product_num,amount)values(?,?,?,?,?,?)";
	public void saveItem(int orderId, List<CartItem> itemsBuy) throws Exception {
		for(CartItem item:itemsBuy){
			Connection conn = DbUtil.getConnection();
			PreparedStatement ps = conn.prepareStatement(saveItem);
			ps.setInt(1,orderId);
			ps.setInt(2, item.getPro().getId());
			ps.setString(3, item.getPro().getProductName());
			ps.setDouble(4, item.getPro().getDangPrice());
			ps.setInt(5, item.getQty());
			ps.setDouble(6, (item.getPro().getDangPrice())*(item.getQty()));
			ps.executeUpdate();
		}
	}
	//保存收货地址
	private final static String saveRecAddress="insert into d_receive_address ( " +
	"user_id,receive_name,full_address,postal_code,mobile,phone" +
	")values(?,?,?,?,?,?)";
	public void saveReceiveAddress(Order order) throws Exception {
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(saveRecAddress);
		stat.setInt(1,order.getUserId());
		stat.setString(2,order.getReceiveName());
		stat.setString(3,order.getFullAddress());
		stat.setString(4,order.getPostalCode());
		stat.setString(5,order.getMobile());
		stat.setString(6,order.getPhone());
		stat.executeUpdate();
	}
	//根据登陆用户的ID查找订单
	private final static String findAddressId="select * from d_receive_address where user_id=?&&id=?";
	public Address findSelectAddress(int userId,int addressId) throws Exception {
		Connection conn = DbUtil.getConnection();
		PreparedStatement stat = conn.prepareStatement(findAddressId);
		stat.setInt(1,userId);
		stat.setInt(2,addressId);
		ResultSet rs = stat.executeQuery();
		Address address = null;
		while(rs.next()){
			address = new Address();
			address.setId(addressId);
			address.setUserId(userId);
			address.setReceiveName(rs.getString("receive_name"));
			address.setFullAddress(rs.getString("full_address"));
			address.setPostalCode(rs.getString("postal_code"));
			address.setMobile(rs.getString("mobile"));
			address.setPhone(rs.getString("phone"));
		}
		return address;
	}

	
}
