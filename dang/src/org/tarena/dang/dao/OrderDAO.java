package org.tarena.dang.dao;

import java.util.List;

import org.tarena.dang.entity.Address;
import org.tarena.dang.entity.CartItem;
import org.tarena.dang.entity.Order;

public interface OrderDAO {
	public void saveOrder(Order order) throws Exception;
	public int findOrderId(int userId) throws Exception;
	
	public void saveItem(int orderId,List<CartItem> itemsBuy) throws Exception;
	
	public List<Address> findAddressById(int userId)throws Exception;
	public void saveReceiveAddress(Order order) throws Exception;
	
	public Address findSelectAddress(int userId,int addressId) throws Exception; 
}
