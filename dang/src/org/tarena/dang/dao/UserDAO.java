package org.tarena.dang.dao;

import org.tarena.dang.entity.User;

public interface UserDAO {
	/**
	 * 用户添加
	 * @param user
	 * @throws Exception
	 */
	public void save(User user) throws Exception;
	/**
	 * 检查email是否被占用
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public User findByEmail(String email) throws Exception;
	
	public void changeVerify(String email) throws Exception;
}
