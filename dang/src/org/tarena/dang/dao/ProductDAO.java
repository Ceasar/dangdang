package org.tarena.dang.dao;

import java.util.List;

import org.tarena.dang.entity.Book;
import org.tarena.dang.entity.Product;
import org.tarena.dang.entity.ProductHot;

public interface ProductDAO {
	public Product findById(int pid) throws Exception;
	//查询最新上架的书
	public List<Product> findNewAll(int size) throws Exception;
	//查询编辑推荐的书
	public List<Book> findRecommendAll() throws Exception;
	//查询热销的书
	public List<ProductHot> fintHotAll() throws Exception;
	//图书热卖榜
	public List<Product> findTopTen() throws Exception;
	/**
	 * 根据类别ID查询所包含的图书信息
	 * @param cid 类别ID
	 * @return 所包含的图书信息
	 * @throws Exception
	 */
	public List<Book> findBooksByCatId(int cid,int page,int size) throws Exception;
	//总页数
	public int getTotalPages(int cid,int size) throws Exception;
}
