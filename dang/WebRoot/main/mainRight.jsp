<%@page isELIgnored="false" contentType="text/html;charset=utf-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<html>
  <head>
    	<link href="../css/book.css" rel="stylesheet" type="text/css" />
		<link href="../css/second.css" rel="stylesheet" type="text/css" />
		<link href="../css/secBook_Show.css" rel="stylesheet" type="text/css" />
  </head>
  <script type="text/javascript">
	$(function(){
		$('.hot').hover(function(){
			var $pro = $(this);
			var pid = $pro.attr('id');
			$.post(
				'findProduct!findProduct.action',
				{'pid':pid},
				function(date){
					$pro.append(
						'<div id="sb" class="second_d_wai" style="position:absolute"><img src=../productImages/'+date.productPic+'>'+
						'<div>'+date.productName+'</div>'+
						'<div>当当价￥'+date.dangPrice+'</div>'+
						'<div>定价￥'+date.fixedPrice+'</div>'+
						'</div>'
					);
				}
			);
		
		},
			function(){
				$('#sb').remove();
			}
		);
	
	});

</script>
  <body>
   <div>
   	<s:iterator value="topList">
		<a class="hot" id="${id }" >&nbsp;&nbsp;&nbsp;&nbsp;${productName }</a><br>
	</s:iterator>
   </div>
  </body>
</html>
