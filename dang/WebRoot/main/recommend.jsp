<%@page isELIgnored="false" contentType="text/html;charset=utf-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<h2>
	编辑推荐
</h2>
<div id=__bianjituijian/danpin>
	<div class=second_c_02>
		<div class=second_c_02_b1>
			<div class=second_c_02_b1_1>
				<a href='#' target='_blank'><img src="../productImages/${rec1.productPic }" width=70 border=0 /> </a>
			</div>
			<div class=second_c_02_b1_2>
				<h3>
					<a href='#' target='_blank' title='${rec1.productName }'>${rec1.productName }</a>
				</h3>
				<h4>
					作者：${rec1.author } &nbsp;&nbsp;著
					<br />
					出版社：${rec1.publishing }&nbsp;&nbsp;&nbsp;&nbsp;出版时间：<s:date name="new java.util.Date(rec1.publishTime)" format="yyyy/MM/dd"/>
				</h4>
				<h5>
					简介:${rec1.catalogue }
				</h5>
				<h6>
					定价：￥${rec1.fixedPrice }&nbsp;&nbsp;当当价：￥${rec1.dangPrice }
				</h6>
				<div class=line_xx></div>
			</div>
		</div>
		
		<div class=second_c_02_b1>
			<div class=second_c_02_b1_1>
				<a href='#' target='_blank'><img src="../productImages/${rec2.productPic }" width=70 border=0 /> </a>
			</div>
			<div class=second_c_02_b1_2>
				<h3>
					<a href='#' target='_blank'  title="${rec2.productName }">${rec2.productName }</a>
				</h3>
				<h4>
					作者：${rec2.author } &nbsp;&nbsp;著
					<br />
					出版社：${rec2.publishing }&nbsp;&nbsp;&nbsp;&nbsp;出版时间：<s:date name="new java.util.Date(rec2.publishTime)" format="yyyy/MM/dd"/>
				</h4>
				<h5>
					简介:${rec2.catalogue }
					<span class="pot">...</span>
				</h5>
				<h6>
					定价：￥${rec2.fixedPrice }&nbsp;&nbsp;当当价：￥${rec2.dangPrice }
				</h6>
			</div>
		</div>
	</div>
</div>
