<%@page contentType="text/html;charset=utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>生成订单 - 当当网</title>
		<link href="../css/login.css" rel="stylesheet" type="text/css" />
		<link href="../css/page_bottom.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/jquery-1.4.3.js"></script>
		<script type="text/javascript">
		function checkReceiveName(){
			$("#nameValidMsg").html("");//清空错误提示
			var receiveNameTxt = $("#receiveName").val();
			if(receiveNameTxt==""){
				$("#nameValidMsg").append("<img src='/dang/images/onError.gif'>收件人不能为空!").css("color","red");
				return false;
			}else{ 
				$("#nameValidMsg").append("<img src='/dang/images/onSuccess.gif'>收件人正确!").css("color","black");
				return true;
			}
		}
		function checkFullAddress(){
			//收件人地址检查
			$("#addressValidMsg").html("");//清空错误提示
			var fullAddressTxt = $("#fullAddress").val();
			if(fullAddressTxt==""){
				$("#addressValidMsg").html("<img src='/dang/images/onError.gif'>地址不能为空!").css("color","red");
				return false;
			}else{
				$("#addressValidMsg").html("<img src='/dang/images/onSuccess.gif'>地址正确!").css("color","black");
				return true;
			}
		}
		function checkPostalCode(){
			$("#codeValidMsg").html("");//清空错误提示
				var postalCodeTxt = $("#postalCode").val();
				var postalCodereg = /^\d{6}$/;
				if(postalCodeTxt==""){
					$("#codeValidMsg").html("<img src='/dang/images/onError.gif'>邮编不能为空!").css("color","red");
					return false;
				}else if(!postalCodereg.test(postalCodeTxt)){
					$("#codeValidMsg").html("<img src='/dang/images/onError.gif'>邮编格式错误!").css("color","red");
					return false;
				}else{
					$("#codeValidMsg").html("<img src='/dang/images/onSuccess.gif'>邮编正确!").css("color","black");
					return true;
				}
			}
			function checkPhone(){
				$("#phoneValidMsg").html("");
				var phoneTxt = $("#phone").val();
				var phonereg = /^[0-9]{8}$/;
				if(phoneTxt==""){
					$("#phoneValidMsg").html("<img src='/dang/images/onError.gif'>电话号码不能为空!").css("color","red");
					return false;
				}else if(!phonereg.test(phoneTxt)){
					$("#phoneValidMsg").html("<img src='/dang/images/onError.gif'>电话号码必须为8位数字!").css("color","red");
					return false;
				}else{
					$("#phoneValidMsg").html("<img src='/dang/images/onSuccess.gif'>电话号码正确!").css("color","black");
					return true;
				}
			}
			function checkMobile(){
				$("#mobileValidMsg").html("");//清空错误提示
				var mobileTxt = $("#mobile").val();
				var mobilereg = /^(13|15)[0-9]{9}$/;
				if(mobileTxt==""){
					$("#mobileValidMsg").html("<img src='/dang/images/onError.gif'>手机号不能为空!").css("color","red");
					return false;
				}else if(!mobilereg.test(mobileTxt)){
					$("#mobileValidMsg").html("<img src='/dang/images/onError.gif'>必须为11位数字!").css("color","red");
					return false;
				}else{
					$("#mobileValidMsg").html("<img src='/dang/images/onSuccess.gif'>手机号正确!").css("color","black");
					return true;
				}
			}
				
			
		//表单项通过检查后才允许提交
		$(function(){
			$("#receiveName").blur(function(){
				checkReceiveName();
			$("#fullAddress").blur(function(){
				checkFullAddress();
			});
			$("#postalCode").blur(function(){
				checkPostalCode();
			});
			$("#phone").blur(function(){
				checkPhone();
			});
			$("#mobile").blur(function(){
				checkMobile();
			});
		});
				
				
			})
			//表单提交事件处理
			$(function(){
				$("#f").submit(function(){
					if(!(receiveName_flag&&fullAddress_flag&&postalCode_flag&&phone_flag&&mobile_flag)){
						alert("表单信息有错或正在检测中...");
						return false;
					}
					return true;
				});
			})
			//页面加载完成后加载地址下拉框
			$(function(){
				$.post(
					"/dang/order/order!loadAddress",
					function(data){
						/*for(var i=0;i<=data.length;i++){
							var ary = data[i];
							alert(ary.fullAddress);
							var opt_str = "<option value='"+ary.id+"'>"+ary.fullAddress+"</option>";
							var $opt = $(opt_str);
							$("#address").append($opt); 
						}*/
						var array = eval(data);
						for(var i=0;i<array.length;i++){
							var opt_str = "<option value='"+array[i].id+"'>"+array[i].fullAddress+"</option>";
							var $opt = $(opt_str);
							$("#address").append($opt);
						}
					}
				);
			});
			//加载已存地址的详细信息
			$(function(){
				$("#address").change(function(){
					var addrId = $("#address").val();
					alert(addrId);
					$.get(
					"/dang/order/order!findSelectAddress",
					{"addressId":addrId},
					function(data){
						if(data==""){
							$("#addressId").val("");
							$("#receiveName").val("");
							$("#fullAddress").val("");
							$("#postalCode").val("");
							$("#phone").val("");
							$("#mobile").val("");
						}else{
							$("#addressId").val(data.id);
							$("#receiveName").val(data.receiveName);
							$("#fullAddress").val(data.fullAddress);
							$("#postalCode").val(data.postalCode);
							$("#phone").val(data.phone);
							$("#mobile").val(data.mobile);
						}
					}
				);
				});
			});
		</script>
	</head>
	<body>
		<%@include file="../common/head1.jsp"%>
		<div class="login_step">
			生成订单骤: 1.确认订单 >
			<span class="red_bold"> 2.填写送货地址</span> > 3.订单成功
		</div>
		<div class="fill_message">
			<p>
				选择地址：
				<select id="address">
					<option value="">
						填写新地址
					</option>
				</select>
			</p>                                     
			<form name="ctl00" method="post" action="order!createOrder.action" id="f">
				<input type="hidden" id="addressId" />

				<table class="tab_login">
					<tr>
						<td valign="top" class="w1">
							收件人姓名：
						</td>
						<td>
							<input type="text" class="text_input" name="order.receiveName"
								id="receiveName" />
							<div class="text_left" id="nameValidMsg">
								<p>
									请填写有效的收件人姓名
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							收件人详细地址：
						</td>
						<td>
							<input type="text" name="order.fullAddress" class="text_input"
								id="fullAddress" />
							<div class="text_left" id="addressValidMsg">
								<p>
									请填写有效的收件人的详细地址
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							邮政编码
						</td>
						<td>
							<input type="text" class="text_input" name="order.postalCode"
								id="postalCode" />
							<div class="text_left" id="codeValidMsg">
								<p>
									请填写有效的收件人的邮政编码
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							电话
						</td>
						<td>
							<input type="text" class="text_input" name="order.phone"
								id="phone" />
							<div class="text_left" id="phoneValidMsg">
								<p>
									请填写有效的收件人的电话
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							手机
						</td>
						<td>
							<input type="text" class="text_input" name="order.mobile"
								id="mobile" />
							<div class="text_left" id="mobileValidMsg">
								<p>
									请填写有效的收件人的手机
								</p>
							</div>
						</td>
					</tr>
				</table>

				<div class="login_in">

					<a href="/dang/order/order!orderInfo.action"><input id="btnClientRegister" class="button_1" name="submit"
					type="button" value="取消" /></a>
			
				<input id="btnClientRegister" class="button_1" name="submit"
					type="submit" value="下一步" />
				</div>
			</form>
		</div>
		<%@include file="../common/foot1.jsp"%>
	</body>
</html>

