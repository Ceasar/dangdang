<%@page contentType="text/html;charset=utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>用户注册 - 当当网</title>
		<link href="../css/login.css" rel="stylesheet" type="text/css" />
		<link href="../css/page_bottom.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/jquery-1.4.3.js"></script>
		<script type="text/javascript">
		var email_flag = false;
		var nickname_flag = false;
		var password_flag = false;
		var repassword_flag = false;
		var code_flag = false;
		//表单校验
		$(function(){
			//邮箱检验
			$("#txtEmail").blur(function(){
				email_flag = false;
				//清空提示消息
				 $("#email\\.info").html("");
				//1.非空检查
				var email = $("#txtEmail").val();
				if(email == ""){
					$("#email\\.info").html("<img src='/dang/images/onError.gif'>邮箱不能为空");
					return;
				}
				//2.格式检查
				var pattern=/\b(^['_A-Za-z0-9-]+(\.['_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\.[A-Za-z0-9-]+)*((\.[A-Za-z0-9]{2,})|(\.[A-Za-z0-9]{2,}\.[A-Za-z0-9]{2,}))$)\b/;
				if(!pattern.test(email)){
					$("#email\\.info").html("<img src='/dang/images/onError.gif'>邮箱格式错误");
					return;
				}
				//3.唯一性检查
				$.post(
					"checkEmail.action",
					{"email":email},
					function(data){//获取返回的ok值
						if(data){//可用
							$("#email\\.info").html("<img src='/dang/images/onSuccess.gif'>邮箱可用");
							email_flag = true;//邮箱通过检测
						}else{//被占用
							$("#email\\.info").html("<img src='/dang/images/onError.gif'>邮箱被占用");
						}
					}
				);
				
			});
		});	
		//给看不清楚链接追加单击处理
		$(function(){
			$("#change").click(function(){
				$("#imgVcode").attr("src","image.action?dt="+new Date().getTime());
				return false;//阻止harf动作
			});
		});
		//检查输入的code是否正确
		$(function(){
			code_flag = false;
			$("#txtVerifyCode").blur(function(){
				var codeTxt = $("#txtVerifyCode").val();
				//判断是否为空
				if(codeTxt==""){
					$("#number\\.info").html("验证码不能为空");
					return;
				}
				$.post("checkcode.action",{"number":codeTxt},function(data){
					if(data){
						$("#number\\.info").html("<img src='/dang/images/onSuccess.gif'>验证码可用");
						code_flag = true;
					}else{
						$("#number\\.info").html("验证码错误");
					}
				});
			});
		});
		$(function(){
			$("#txtNickName").blur(function(){
				nickname_flag = false;
				$("#name\\.info").html("");
				var NickTxt = $("#txtNickName").val();
				if(NickTxt==""){
					$("#name\\.info").html("<img src='/dang/images/onError.gif'>昵称不能为空");
					return;
				}
				$("#name\\.info").html("<img src='/dang/images/onSuccess.gif'>昵称可用");		
				nickname_flag = true;
			});
		});
		$(function(){
			$("#txtPassword").blur(function(){
				password_flag = false;
				$("#password\\.info").html("");
				var passwordTxt = $("#txtPassword").val();
				if(passwordTxt==""){
					$("#password\\.info").html("<img src='/dang/images/onError.gif'>密码不能为空");
					return;
				}
				$("#password\\.info").html("<img src='/dang/images/onSuccess.gif'>密码可用");
				password_flag = true;
			});
		});
		$(function(){
			$("#txtRepeatPass").blur(function(){
				repassword_flag = false;				
				$("#password1\\.info").html("");
				var passwordTxt = $("#txtPassword").val();
				var repasswordTxt = $("#txtRepeatPass").val();
				if(repasswordTxt==""){
					$("#password1\\.info").html("<img src='/dang/images/onError.gif'>此项必须和输入的密码相同");
					return;
				}
				else if(passwordTxt!=repasswordTxt){
					$("#password1\\.info").html("<img src='/dang/images/onError.gif'>此项必须和输入的密码相同");
					return;
				}
				$("#password1\\.info").html("<img src='/dang/images/onSuccess.gif'>可用");
				repassword_flag = true;
			});
		})
		//表单提交事件处理
		$(function(){
			$("#register_form").submit(function(){
				//判断各个表单项是否通过检查
			 //	return email_flag&&nickname_flag&&;
				if(!(email_flag&&code_flag&&nickname_flag&&password_flag&&repassword_flag)){
					alert("表单信息有错或正在检测中...");
					return false;
				}
				return true;
			});
		});
		$(function(){
			$("#txtEmail").focus(function(){
				$("#txtEmail").val("");
				$("#email\\.info").html("");
			});
			$("#txtNickName").focus(function(){
				$("#name\\.info").html("");
			});
			$("#txtPassword").focus(function(){
				$("#password\\.info").html("");
			});
			$("#txtRepeatPass").focus(function(){
				$("#password1\\.info").html("");
			});
		});
	</script>
	</head>
	<body>
		<%@include file="../common/head1.jsp"%>
		<div class="login_step">
			注册步骤:
			<span class="red_bold">1.填写信息</span> > 2.验证邮箱 > 3.注册成功
		</div>
		<div class="fill_message">
			<form name="ctl00" method="post" action="register.action" id="register_form">
				<h2>
					以下均为必填项
				</h2>
				<table class="tab_login" >
					<tr>
						<td valign="top" class="w1">
							请填写您的Email地址：
						</td>
						<td>
							<input name="user.email" type="text" id="txtEmail" class="text_input"/>
							<div class="text_left" id="emailValidMsg">
								<p>
									请填写有效的Email地址，在下一步中您将用此邮箱接收验证邮件。
								</p>
									<span id="email.info" style="color:red"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							设置您在当当网的昵称：
						</td>
						<td>
							<input name="user.nickname" type="text" id="txtNickName" class="text_input" />
							<div class="text_left" id="nickNameValidMsg">
								<p>
									您的昵称可以由小写英文字母、中文、数字组成，
								</p>
								<p>
									长度4－20个字符，一个汉字为两个字符。
								</p>
								<span id="name.info" style="color:red"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							设置密码：
						</td>
						<td>
							<input name="user.password" type="password" id="txtPassword"
								class="text_input" />
							<div class="text_left" id="passwordValidMsg">
								<p>
									您的密码可以由大小写英文字母、数字组成，长度6－20位。
								</p>
								<span id="password.info" style="color:red"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							再次输入您设置的密码：
						</td>
						<td>
							<input name="password1" type="password" id="txtRepeatPass"
								class="text_input"/>
							<div class="text_left" id="repeatPassValidMsg">
							<span id="password1.info" style="color:red"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" class="w1">
							验证码：
						</td>
						<td>
							<img class="yzm_img" id='imgVcode' src="image.action" />
							<input name="number" type="text" id="txtVerifyCode" class="yzm_input"/>
							<div class="text_left t1">
								<p class="t1">
									<span id="vcodeValidMsg">请输入图片中的四个字母。</span>
									
									<span id="number.info" style="color:red"></span>
									<a href="#" id="change">看不清楚？换个图片</a>
								</p>
							</div>
						</td>
					</tr>
				</table>

				<div class="login_in">

					<input id="btnClientRegister" class="button_1" name="submit"  type="submit" value="注 册"/>
				</div>
			</form>
		</div>
		<%@include file="../common/foot1.jsp"%>
	</body>
</html>

